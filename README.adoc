= AmigaDOS - A Dabhand Guide

This is the digital version of the book `AmigaDOS - A Dabhand Guide`

*Author:* Mark Burgess

*Year:* 1990

*Publisher:* Dabs Press

Released under *Creative Commons* 2018.

== Description

_An overview om AmigaDOS_

== Read this book: 

* https://gitlab.com/amigasourcecodepreservation/amigados-a-dabhand-guide/blob/master/pdf/amigados-a-dabhand-guide-1990-burgess.pdf[Scanned pdf]

== Contributing

If you'd like to help out, here is a suggested to-do:

* Convert the original scan to AsciiDoc.
 
== License

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/

Many thanks to Mark Burgess, for being an friend of open information.

== Copyright & Author

Mark Burgess © 1990 - 2018


